---
author: Kordian Galiński
title: Francja - Prezentacja
subtitle: Beamer
date: 06.11.2023
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Slajd 1: Wprowadzenie do Francji
![Zdjęcie Wieży Eiffla](assets/wieza.png)
- Oficjalna nazwa: Republika Francuska
- Położenie: Zachodnia Europa
- Stolica: Paryż
- Język urzędowy: Francuski
- Waluta: Euro (€)

## Slajd 2: Geografia Francji
![Mapa Francji](assets/mapa.jpg)
- Zróżnicowany krajobraz: Alpy, Pireneje, niziny, wybrzeża
- Rzeki: Sekwana, Loara, Rodan
- Klimat: Od umiarkowanego po śródziemnomorski

## Slajd 3: Historia Francji
![Zdjęcie Zamku w Loarze](assets/loara.png)
- Galowie, Rzymianie, Frankowie
- Rewolucja Francuska w 1789 roku
- V Republika od 1958 roku

## Slajd 4: Kultura i Sztuka Francuskiej
![Zdjęcie Luwru](assets/luwr.png)
- Literatura: Victor Hugo, Marcel Proust
- Malarstwo: Monet, Cezanne
- Filozofia: Descartes, Sartre

## Slajd 5: Kuchnia Francuska
![Zdjęcie francuskich serów i wina](assets/sery.jpg)
- Słynie z serów, wina i pieczywa
- Typowe dania: Bouillabaisse, Ratatouille
- Restauracje z gwiazdkami Michelin

## Slajd 6: Gospodarka Francji
![Zdjęcie francuskiego samolotu Airbus](assets/airbus.jpg)
- Członek Unii Europejskiej i G7
- Silne sektory: rolnictwo, przemysł lotniczy i obronny, moda
- Turystyka: jedna z najczęściej odwiedzanych krajów

## Slajd 7: Znane Zabytki i Miejsca
![Zdjęcie Mont Saint-Michel](assets/stmichel.png)
- Wieża Eiffla, Katedra Notre-Dame, Pałac Wersalski
- Regiony winiarskie: Bordeaux, Burgundia
- Disneyland Paryż

## Slajd 8: Współczesna Francja
![Zdjęcie współczesnego Paryża](assets/paryz.jpg)
- Polityka: prezydencka republika semi-prezydencka
- Społeczeństwo: wielokulturowość i globalizacja
- Wyzwania: integracja europejska, zmiany klimatyczne